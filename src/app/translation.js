

import resources from '../app/localization/localize';

i18next.init({
    lng: 'en',
    resources,
  }, function(err, t) {
    i18nextJquery.init(i18next, $);
    
    $('.container').localize();

    $('.lang-select').click(function() {
      console.log(this.id);
      i18next.changeLanguage(this.id);
      $('.container').localize();
     
    });
  });