const lazyLoadImage = (imageName, img) =>{
    import(
        `../static/${imageName}`
    ).then(src=> img.src = src.default)
    .catch(err=>console.log(errr))
};
export default lazyLoadImage;
