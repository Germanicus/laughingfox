import getImagesNames from './getImageNames';
import generateImage from './generateImage';

const images = document.querySelectorAll('.portfolio__box--image');
const imageName = getImagesNames();
let imagesLength=0;
images.forEach((name) =>  generateImage(name, imageName[imagesLength++]));