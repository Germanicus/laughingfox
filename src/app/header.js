
let mainNav = document.getElementById('js-menu');
let navBarToggle = document.getElementById('js-navbar-toggle');

navBarToggle.addEventListener('click', function () {
    
     mainNav.classList.toggle('active');
  
});

$(window).bind('scroll', ()=>{
     if($(window).scrollTop()<=40)
     {
          $('.header').removeClass('fixed');
     }
     else{
          $('.header').addClass('fixed');
     }
})