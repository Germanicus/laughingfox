export default () =>{
    const r= require.context('../static', false, /\.(gif|jpg|jpeg|tiff|png)$/);
    const importAll= (r) => r.keys().map(file => file.match(/[^\/]+$/)[0])
    return importAll(r);
 }