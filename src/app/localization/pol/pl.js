const pl= {
    translation: {
      nav: {
        page1: 'STRONA GŁÓWNA',
        page2: 'NASZE GRY',
        page3: 'O NAS',
        page4: 'KONTAKT'
      },
      header:{
        title:"GRY KOMPUTEROWE SĄ NASZĄ PASJĄ "
    },
    about:{
      name: "O nas"
    },
    contact:{
      name: "Skontaktuj się z nami:"
    },
    game: {
      name:"NASZA GRA"
    },
    studio:{
      text:`Czy słyszałeś kiedyś jaki dźwięk wydaje śmiejący się lis? Jest to jeden z najbardziej uroczych dźwięków na świecie.
      Właśnie dlatego nazwaliśmy nasze studio "Laughing fox games". 
      Jesteśmy małym niezależny studiem indie z wielką pasją do gier. Właśnie kończymy naszą pierwszą grę "Rescue
      Bomber", a w planach mamy już kolejne. Chcemy aby ludzie grający w nasze gry mogli się śmiać radośnie niczym lisy :)`
    }
    }
    
  }
  export default pl;