import de from "./de/de";
import en from "./en/en";
import pl from "./pol/pl";
const localization = {
  en,
  de,
  pl
};
export default localization;
