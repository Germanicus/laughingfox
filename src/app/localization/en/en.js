const en = {
    translation: {
      nav: {
        page1: 'HOME',
        page2: 'OUR GAMES',
        page3: 'ABOUT US',
        page4: 'CONTACT',
      },
      header:{
        title:"GAME DEV IS OUR PASSION"
    },
    about:{
      name: "About Us",
      
    },
    contact:{
      name: "Contact Us at:"
    },
    game: {
      name:"OUR GAME"
    },
    studio:{
      text:`Have you ever heard the sound that fox makes when laughing? It is one of the cutest sounds in the world!
      That's why we named our studio "Laughing fox games". We are small independent indie studio with big passion for games.
      At the moment we are finishing our first game "Rescue Bomber", and we already have plans for next games.
      We want to people playing our game to laugh as joyful as foxes do :) `
    }

    }
  }
  export default en;